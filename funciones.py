def setHeader(msg,wrp):
    message=str(msg)
    padding = len(msg)//2
    wrapper = str(wrp)*padding*4
    header = message.center(padding * 4)

    return wrapper + "\n" + header + "\n" + wrapper + "\n"

if __name__ == "__main__":
    print(setHeader("Hey there!","#"))
